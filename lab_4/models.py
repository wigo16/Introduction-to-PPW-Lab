from django.db import models
from django.utils import timezone
import pytz

class Message(models.Model):
        def converted_timezone():
                return timezone.now() + timezone.timedelta(hours=7)

        name = models.CharField(max_length=27)
        email = models.EmailField()
        message = models.TextField(error_messages={'required': 'Please enter your name'})
        created_date = models.DateTimeField(default=converted_timezone)

        def __str__(self):
        	return self.message
