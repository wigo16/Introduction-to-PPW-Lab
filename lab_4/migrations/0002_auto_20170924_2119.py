# -*- coding: utf-8 -*-
# Generated by Django 1.11.5 on 2017-09-24 14:19
from __future__ import unicode_literals

from django.db import migrations, models
import lab_4.models


class Migration(migrations.Migration):

    dependencies = [
        ('lab_4', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='message',
            name='created_date',
            field=models.DateTimeField(default=lab_4.models.Message.converted_timezone),
        ),
    ]
