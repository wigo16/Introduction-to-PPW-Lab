window.fbAsyncInit = function() {
  FB.init({
    appId      : '349234305540991',
    cookie     : true,
    xfbml      : true,
    version    : 'v2.11'
  });
    
  FB.AppEvents.logPageView();

  // implementasilah sebuah fungsi yang melakukan cek status login (getLoginStatus)
  // dan jalankanlah fungsi render di bawah, dengan parameter true jika
  // status login terkoneksi (connected)

  // Hal ini dilakukan agar ketika web dibuka dan ternyata sudah login, maka secara
  // otomatis akan ditampilkan view sudah login
  FB.getLoginStatus(function(response){
    if (response.status === 'connected') {
      var loginFlag = true;
    }else if(response.status === 'not_authorized'){
      var loginFlag = false;
    }else{
      var loginFlag = false;
    }
    render(loginFlag);
  })   
    
};

(function(d, s, id){
   var js, fjs = d.getElementsByTagName(s)[0];
   if (d.getElementById(id)) {return;}
   js = d.createElement(s); js.id = id;
   js.src = "https://connect.facebook.net/en_US/sdk.js";
   fjs.parentNode.insertBefore(js, fjs);
 }(document, 'script', 'facebook-jssdk'));


// Fungsi Render, menerima parameter loginFlag yang menentukan apakah harus
// merender atau membuat tampilan html untuk yang sudah login atau belum
// Ubah metode ini seperlunya jika kalian perlu mengganti tampilan dengan memberi
// Class-Class Bootstrap atau CSS yang anda implementasi sendiri
const render = loginFlag => {
  if (loginFlag) {
    // Jika yang akan dirender adalah tampilan sudah login
    // Memanggil method getUserData (lihat ke bawah) yang Anda implementasi dengan fungsi callback
    // yang menerima object user sebagai parameter.
    // Object user ini merupakan object hasil response dari pemanggilan API Facebook.
    getUserData(user => {
      // Render tampilan profil, form input post, tombol post status, dan tombol logout
      $('#lab8').html(
        '<div class="profile">' +
          '<img class="cover" src="' + user.cover.source + '" alt="cover" />' +
          '<img class="picture" src="' + user.picture.data.url + '" alt="profpic" />' +
          '<div class="data">' +
            '<h1>' + user.name + '</h1>' +
            '<h5>' + user.about + '</h5>' +
            '<h3>' + user.email + ' - ' + user.gender + '</h3>' +
          '</div>' +
        '</div>' +
        '<form>' +
          '<div class="form-group">' +
            '<label for="status">Status anda: </label>' +
            '<textarea id="postInput" style="resize:none" class="form-control" rows="6" column="3" id="status"></textarea>' +
          '</div>' +
          '<button onclick="postStatus()" class="next btn btn-primary" type="submit">Post</button>' +
        '</form>' +
        '<br>' +
        '<button class="next btn btn-danger" onclick="facebookLogout()">Logout</button>'
      );

      // Setelah merender tampilan di atas, dapatkan data home feed dari akun yang login
      // dengan memanggil method getUserFeed yang kalian implementasi sendiri.
      // Method itu harus menerima parameter berupa fungsi callback, dimana fungsi callback
      // ini akan menerima parameter object feed yang merupakan response dari pemanggilan API Facebook
      getUserFeed(feed => {
        feed.data.map(value => {
          // Render feed, kustomisasi sesuai kebutuhan.
          if (value.message && value.story) {
            $('#lab8').append(
              '<div class="list-group">' +
                '<a href="#" class="list-group-item">' + value.message + '</a>' +
                '<a href="#" class="list-group-item">' + value.story + '</a>' +
              '</div>'
            );
          } else if (value.message) {
            $('#lab8').append(
              '<div class="list-group">' +
                '<a href="#" class="list-group-item">' + value.message + '</a>' +
              '</div>'
            );
          } else if (value.story) {
            $('#lab8').append(
              '<div class="list-group">' +
                '<a href="#" class="list-group-item">' + value.story + '</a>' +
              '</div>'
            );
          }
        });
      });
    });
  } else {
    // Tampilan ketika belum login
    $('#lab8').html(
        '<div id="welcome">' +
          '<h1>' + '<strong>' + 'Hi,' + '</strong>' + '</h1>' +
          '<h4>' + 'WELCOME TO MY WEBSITE' + '</h4>' +
        '</div>' +
        '<div id="facebookLogin" >' +
          '<button id="facebookLoginButton" onclick="facebookLogin()"><strong>Login with Facebook</strong></button>' +
        '</div>'
      );
  }
};

const facebookLogin = () => {
  // TODO: Implement Method Ini
  // Pastikan method memiliki callback yang akan memanggil fungsi render tampilan sudah login
  // ketika login sukses, serta juga fungsi ini memiliki segala permission yang dibutuhkan
  // pada scope yang ada. Anda dapat memodifikasi fungsi facebookLogin di atas.
  FB.login(function(response){
      console.log(response);
      location.reload();
     },
     {scope:'public_profile,user_posts,publish_actions,email,user_about_me'})
};

const facebookLogout = () => {
  // TODO: Implement Method Ini
  // Pastikan method memiliki callback yang akan memanggil fungsi render tampilan belum login
  // ketika logout sukses. Anda dapat memodifikasi fungsi facebookLogout di atas.
  FB.getLoginStatus(function(response) {
        if (response.status === 'connected') {
          FB.logout(function(){
            location.reload();
          });
        }
     });
};

// TODO: Lengkapi Method Ini
// Method ini memodifikasi method getUserData di atas yang menerima fungsi callback bernama fun
// lalu merequest data user dari akun yang sedang login dengan semua fields yang dibutuhkan di 
// method render, dan memanggil fungsi callback tersebut setelah selesai melakukan request dan 
// meneruskan response yang didapat ke fungsi callback tersebut
// Apakah yang dimaksud dengan fungsi callback?
const getUserData = (fun) => {
  FB.api('/me?fields=id,name,email,about,picture,gender,cover', 'GET', function (response){
    fun(response);
  });
};

const getUserFeed = (fun) => {
  // TODO: Implement Method Ini
  // Pastikan method ini menerima parameter berupa fungsi callback, lalu merequest data Home Feed dari akun
  // yang sedang login dengan semua fields yang dibutuhkan di method render, dan memanggil fungsi callback
  // tersebut setelah selesai melakukan request dan meneruskan response yang didapat ke fungsi callback
  // tersebut
  FB.api('/me/feed', function(response) {
      if (!response || response.error) {
        alert(response.error);
      } else {
        fun(response)
      }
    }
  );
}

const postFeed = (message) => {
  // Todo: Implement method ini,
  // Pastikan method ini menerima parameter berupa string message dan melakukan Request POST ke Feed
  // Melalui API Facebook dengan message yang diterima dari parameter.
  FB.api('/me/feed', 'POST', {message:message});
  location.reload();
};

const postStatus = () => {
  const message = $('#postInput').val();
  if (message == '') {
    alert("Status tidak boleh kosong")
  }else{
    postFeed(message);
    //$('#postInput').val('');
  }
};