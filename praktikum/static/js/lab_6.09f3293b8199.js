var print = document.getElementById('print');
var erase = false;

function go(x) {
	if (x == 'ac') {
		if(erase == true){
	 		print.value = null;
	 		erase = false;
		}
	} else if (x == 'eval') {
		print.value = Math.round(eval(print.value) * 10000) / 10000;
	} else if (x == 'sin') {
		var deg = eval(print.value);
		var rad = deg * (Math.PI / 180);
	    	print.value = Math.round(Math.sin(rad) * 10000) / 10000;
	} else if (x == 'tan') {
	   	var deg = eval(print.value);
	   	var rad = deg * (Math.PI / 180);
	   	if((deg/90)%2==1){
	   		print.value = 'Invalid Input';
	   	} else{
	   		print.value = Math.round(Math.tan(rad) * 10000) / 10000;
	   	} 
	} else if (x == 'log') {
		   	print.value = Math.log10(Math.round(eval(print.value) * 10000) / 10000);
	} else {
		erase = true;
		if(print.value == null) {
			print.value = x;
		} else {
			print.value += x;
		}
	}
};

  function eval(fn) {
    return new Function('return ' + fn)();
  }

//Chat field
  var submit = document.getElementById('submittedMsg');
  var message = document.getElementById('msgToSubmit');
  var numsChat = 0;

  if(localStorage.historyChat) {
      submit.innerHTML = localStorage.historyChat;    
  } else {
    localStorage.historyChat = "";
  }

  function sumbitMsg(event) {
    var keyPressed = event.keyCode || event.which;
    //if ENTER is pressed
    if(keyPressed==13) {
      if(message.innerHTML != null || message.innerHTML != "") {
        if(numsChat % 2 == 0){
          var cls = "msg-send";
          var sender = "Wigo: ";
          var userMessage = sender + message.value;
          localStorage.historyChat = localStorage.historyChat + '<p class="'+cls+'">'+userMessage+'</p>';
          submit.innerHTML = localStorage.historyChat;
          message.value = null;
          numsChat+=1; '<p class="msg-send">userMessage</p>'
        } 
        else {
          var cls = "msg-receive";
          var sender = "Ogiw: ";
          var userMessage = sender + message.value;
          localStorage.historyChat = localStorage.historyChat + '<p class="'+cls+'">'+userMessage+'</p>';
          submit.innerHTML = localStorage.historyChat;
          message.value = null;
          numsChat+=1;
        }
      }
    }
  }

  function hideChat() {
  var chatBox = document.getElementById('chat-body');
    if(chatBox.style.display === "block") {
      chatBox.style.display = "none";
      document.getElementById('arrowIcon').style.transform = "rotateZ(180deg)";
      document.getElementById('arrowIcon').title = "Show Chat";
    } else {
      chatBox.style.display = "block"
      document.getElementById('arrowIcon').style.transform = "rotateZ(0deg)";
      document.getElementById('arrowIcon').title = "Hide Chat";
    }
  }

//Chnage Theme field
if(typeof(Storage) !== "undefined") {
	var themes = [
	    {"id":0,"text":"Red","bcgColor":"#F44336","fontColor":"#FAFAFA"},
	    {"id":1,"text":"Pink","bcgColor":"#E91E63","fontColor":"#FAFAFA"},
	    {"id":2,"text":"Purple","bcgColor":"#9C27B0","fontColor":"#FAFAFA"},
	    {"id":3,"text":"Indigo","bcgColor":"#3F51B5","fontColor":"#FAFAFA"},
	    {"id":4,"text":"Blue","bcgColor":"#2196F3","fontColor":"#212121"},
	    {"id":5,"text":"Teal","bcgColor":"#009688","fontColor":"#212121"},
	    {"id":6,"text":"Lime","bcgColor":"#CDDC39","fontColor":"#212121"},
	    {"id":7,"text":"Yellow","bcgColor":"#FFEB3B","fontColor":"#212121"},
	    {"id":8,"text":"Amber","bcgColor":"#FFC107","fontColor":"#212121"},
	    {"id":9,"text":"Orange","bcgColor":"#FF5722","fontColor":"#212121"},
	    {"id":10,"text":"Brown","bcgColor":"#795548","fontColor":"#FAFAFA"}
		];
	localStorage.themes = JSON.stringify(themes);
} else {
	window.alert("I'm sorry, you can't do that.")
}

$(document).ready(function() {
	if(localStorage.selectedTheme){
  		var themeNow = JSON.parse(localStorage.selectedTheme);
  		$('body').css({"background-color" : themeNow.Indigo.bcgColor, "color" : themeNow.Indigo.fontColor});
  	} else {
		var selectedTheme = {"Indigo":{"bcgColor":"#3F51B5","fontColor":"black"}}
		localStorage.selectedTheme = JSON.stringify(selectedTheme);
		$('body').css({"background-color" : selectedTheme.Indigo.bcgColor, "color" : themeNow.Indigo.fontColor});
  	}

    $('.my-select').select2({
    	'data' : JSON.parse(localStorage.themes),
    })

    $('.apply-button').on('click', function() {
    	var theme_value = $('.my-select').val();
    	var selected_Theme = themes[theme_value];
    	$('body').css({"background-color" : selected_Theme.bcgColor, "color" : selected_Theme.fontColor});
    	var text_color = selected_Theme.text;
    	var background_color = selected_Theme.bcgColor;
    	var font_color = selected_Theme.fontColor;
    	var test = {"Indigo":{"bcgColor":background_color,"fontColor":font_color}};
    	localStorage.selectedTheme = JSON.stringify(test);
    })
});
