// Calculator
var print = document.getElementById('print');
var erase = false;

function go(x) {
  if (x == 'ac') {
    print.value = null;
  } else if (x == 'eval') {
      print.value = Math.round(eval(print.value) * 10000) / 10000;
      erase = true;
  } else {
    print.value += x;
  }
};

function eval(fn) {
  return new Function('return ' + fn)();
}
// END
