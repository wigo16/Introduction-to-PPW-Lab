from django.db import models

class Friend(models.Model):
   friend_name = models.CharField(max_length=400)
   npm = models.CharField(max_length=250)
   address = models.CharField(max_length=400, default=" ")
   postcode = models.CharField(max_length=100, default=" ")
   hometown = models.CharField(max_length=250, default=" ")
   birthday = models.CharField(max_length=250, default=" ")
   course = models.CharField(max_length=250, default=" ")
   batch = models.CharField(max_length=250, default=" ")
   added_at = models.DateField(auto_now_add=True)