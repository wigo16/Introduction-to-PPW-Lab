from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest, HttpResponse
from django.core import serializers
import json
from .views import index, paginate_page, friend_list, add_friend, friend_description, delete_friend, friend_list_json, validate_npm, model_to_dict
from .models import Friend
from .api_csui_helper.csui_helper import CSUIhelper

class Lab7UnitTest(TestCase):
    def test_lab_7_url_is_exist(self):
        response = Client().get('/lab-7/')
        self.assertEqual(response.status_code, 200)
	
    def test_lab_7_using_index_func(self):
        found = resolve('/lab-7/')
        self.assertEqual(found.func, index)

    def test_lab_7_get_friend_list_view(self):
        response = Client().get('/lab-7/get-friend-list/')
        html_response = response.content.decode('utf8')
        self.assertIn('My Fasilkom Friend', html_response)

    def test_lab_7_friend_description_view(self):
        new_friend = Friend.objects.create(friend_name='Wigo', npm='1606878215')
        id_new_friend = new_friend.id
        found = resolve('/lab-7/get-friend-list/description/' + str(id_new_friend) + '/')

        self.assertEqual(found.func, friend_description)
        response = Client().get('/lab-7/get-friend-list/description/' + str(id_new_friend) + '/')
        html_response = response.content.decode('utf8')
        self.assertIn(new_friend.npm, html_response)        
        
    def test_lab_7_can_add_friend(self):
        response = Client().post('/lab-7/add-friend/', {'name':'Wigo', 'npm':'1606878215', 
            'address':'', 'postcode':'', 'hometown':'', 'birthday':'', 'course':'', 'batch':''})
        self.assertEqual(response.status_code, 200) 

        response = Client().get('/lab-7/get-friend-list/')
        html_response = response.content.decode('utf8')
        self.assertIn("Wigo", html_response)

    def test_lab_7_fail_add_friend(self):
        response = Client().post('/lab-7/add-friend/', {'name':'', 'npm':'', 
            'address':'', 'postcode':'', 'hometown':'', 'birthday':'', 'course':'', 'batch':''})
        self.assertEqual(response.status_code, 500)

    def test_lab_7_can_delete_friend(self):
        new_friend = Friend.objects.create(friend_name='Wigo', npm='1606878215')

        # Retrieving all available activity
        pre_counting_all_available_todo = Friend.objects.all().count()
        self.assertEqual(pre_counting_all_available_todo, 1)

        id_new_friend = new_friend.id

        delete_friend("request", id_new_friend)
        post_counting_all_available_todo = Friend.objects.all().count()
        self.assertEqual(post_counting_all_available_todo, 0)

    def test_lab_7_can_convert_model_to_dict(self):
        friend = Friend.objects.create(friend_name='Wigo', npm='1606878215')
        friend.save()
        data1 = model_to_dict(friend)

        data2 = serializers.serialize('json', [friend,])
        struct = json.loads(data2)
        data2 = json.dumps(struct[0]["fields"])
        self.assertEqual(data1, data2)

    def test_lab_7_can_convert_models_to_json(self):
        data = friend_list_json("request")
        self.assertEqual(data.status_code, 200)

    def test_lab_7_auth_param_dict(self):
        csui_helper = CSUIhelper()
        auth_param = csui_helper.instance.get_auth_param_dict()
        self.assertEqual(auth_param['client_id'], csui_helper.instance.get_auth_param_dict()['client_id'])

    # def test_lab_7_validate_npm(self):
    #     response = self.client.post('/lab-7/validate-npm/')
    #     html_response = response.content.decode('utf8')
    #     self.assertEqual(response.status_code, 200)
    #     self.assertJSONEqual(html_response, {'is_taken': False})

    # def test_lab_7_invalid_sso(self):
    #     username = 'Wigo'
    #     password = '123456'
    #     csui_helper = CSUIhelper()

    #     with self.assertRaises(Exception) as context:
    #         csui_helper.instance.get_access_token(username, password)
    #     self.assertIn('Wigo', str(context.exception))
    