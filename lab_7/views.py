from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from django.views.decorators.csrf import csrf_exempt
from django.core import serializers
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

from .models import Friend
from .api_csui_helper.csui_helper import CSUIhelper
import os
import json

csui_helper = CSUIhelper()
response = {}

def index(request):

    page = request.GET.get('page', 1)
    auth = csui_helper.instance.get_auth_param_dict()

    mahasiswa_list, has_prev, has_next = csui_helper.instance.get_mahasiswa_list(page)
    friend_list = Friend.objects.all()

    response['author'] = 'Ahmad Wigo Prasetya'
    response['mahasiswa_list'] = mahasiswa_list
    response['friend_list'] = friend_list
    response['has_next'] = has_next
    response['has_prev'] = has_prev
    response['page'] = page
    response['auth'] = auth
    response['max_page'] = 68

    html = 'lab_7/lab_7.html'

    return render(request, html, response)

def paginate_page(page, mahasiswa_list):
    paginator = Paginator(mahasiswa_list, 15)
    try:
        data = paginator.page(page)
    except PageNotAnInteger:
        data = paginator.page(1)
    except EmptyPage:
        data = paginator.page(paginator.num_pages)

    index = data.number - 1
    max_index = len(paginator.page_range)
    if index >= 10:
        start_index = index
    else:
        start_index = 0

    if index < max_index - 10:
        end_index = 10
    else:
        end_index = max_index

    page_range = list(paginator.page_range)[start_index:end_index]
    paginataion_data = {'data': data, 'page_range': page_range}
    return paginataion_data

def friend_list(request):
    friend_list = Friend.objects.all()

    page = request.GET.get('page', 1)
    paginataion_data = paginate_page(page, friend_list)
    mahasiswa = paginataion_data['data']
    page_range = paginataion_data['page_range']

    response = {"friend_list": mahasiswa, "page_range": page_range}
    html = 'lab_7/daftar_teman.html'
    return render(request, html, response)

def friend_list_json(request):
    friends = Friend.objects.all()
    return HttpResponse(serializers.serialize("json", friends))


@csrf_exempt
def add_friend(request):
    if request.method == 'POST':
        name = request.POST['name']
        npm = request.POST['npm']
        address = request.POST['address']
        postcode = request.POST['postcode']
        hometown = request.POST['hometown']
        birthday = request.POST['birthday']
        course = request.POST['course']
        batch = request.POST['batch']
        name_exist = Friend.objects.filter(friend_name=name).count()
        npm_exist = Friend.objects.filter(npm=npm).count()
        if (npm_exist > 0) or (name == "" or npm == ""):
            return HttpResponse(status=500)
        else:
            friend = Friend(friend_name=name, npm=npm, address=address, postcode=postcode, hometown=hometown, birthday=birthday, course=course, batch=batch)
            friend.save()
            data = model_to_dict(friend)
            return HttpResponse(data)

def friend_description(request, friend_id):
    friend = Friend.objects.filter(id=friend_id)[0]
    response['friend'] = friend
    html = 'lab_7/deskripsi_teman.html'
    return render(request, html, response)

@csrf_exempt
def delete_friend(request, friend_id):
    Friend.objects.filter(id=friend_id).delete()
    return HttpResponse(status=204)

@csrf_exempt
def validate_npm(request):
    data = {'is_taken': False}
    npm = request.POST['npm']
    npm_exist = Friend.objects.filter(npm=npm).count()
    if npm_exist > 0:
        data = {'is_taken': True}

    return JsonResponse(data)

def model_to_dict(obj):
    data = serializers.serialize('json', [obj,])
    struct = json.loads(data)
    data = json.dumps(struct[0]["fields"])
    return data